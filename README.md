# Postman Setup

Postman Setup documentation for the Bouc.io project


## Getting started

### Creating the new environment...

First, Postman will need the local variables required to run the various endpoints provided within the Bouc.io collection.  To do so, first create a new environment by clicking on "Environments" and then on '+'.  Provide a name for your environment.  Within your new environment, create the following variables along their respective values:

- authn.uri: authn.app.docker.internal
- authz.uri: authz.app.docker.internal
- test.api.uri: test.api.docker.internal
- jwtClient: oauth2-proxy
- jwtClientSecret: <value provided within Keycloak>
- test_username: user-1
- test_password: user-1
- oauth_redirect: https%3A%2F%2Fauthz.app.docker.internal%2Foauth2%2Fcallback
- oauth_state: hf41J_6uRIPLrOO_pUrAQIcYI4seyf14_6ua____-ZE
- refresh_token: <value will be automatically assigned by one of the endpoint>
- access_token: <value will be automatically assigned by one of the endpoint>
- redirect: https://oauth.pstmn.io/v1/callback

### Creating the collection...

Second, you will need the Bouc.io collection, containing all requests currently available.  To do so, import the collection provided in JSON format by uploading it within Postman.

Within your workspace, click on "import", then "upload files" and selecting the "Bouc.io.postman_collection.json" file provided, click "open".


## Executing Test

In order to access the API endpoint example, you must first obtain the proper cookie provided by OAuth2-Proxy, which is set once the IDP (Keycloak in our installation) has authenticated the user.  To do so, from your collection main tab, click on "Get New Access Token".  You should be prompted to log in.

Once successful, double-click on the "Test API Endpoint" example provided and click "Send".  You should receive the list of 4 test, dummy users coded within the example in JSON format.


## Sequence Diagrams for Reference

Sequence diagram for the Bouc.io solution:
![Reference: https://medium.com/@senthilrch/api-authentication-using-istio-ingress-gateway-oauth2-proxy-and-keycloak-a980c996c259](./Oauth2.0-sequence-diagram-Istio-OAuth2-proxy-Keycloak.png)

Sequence diagram for Oauth2 Authorization Code:
![Reference: ](./Oauth2.0-Authorization-Code.png)


## References

- https://www.baeldung.com/postman-keycloak-endpoints
- https://medium.com/@senthilrch/api-authentication-using-istio-ingress-gateway-oauth2-proxy-and-keycloak-a980c996c259
